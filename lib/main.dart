import 'package:flutter/material.dart';
import 'package:spotify_clone/pages/home.page.dart';
import 'package:spotify_clone/pages/library.page.dart';
import 'package:spotify_clone/pages/search.page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static const ROUTE_HOME = '/home';
  static const ROUTE_SEARCH = '/search';
  static const ROUTE_LIBRARY = '/library';

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Spotify Clone',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.black54,
      ),
      home: MyHomePage(title: 'Spotify'),
      initialRoute: ROUTE_HOME,
      onGenerateRoute: (RouteSettings settings){
        return MaterialPageRoute(
            settings: settings,
            builder: (BuildContext context){
              switch(settings.name){
                case ROUTE_HOME:
                  return MyHomePage(title: "Spotify Home",);
                case ROUTE_SEARCH:
                  return SearchPage(title: "Spotify Search");
                case ROUTE_LIBRARY:
                  return LibraryPage(title: "Spotify Library");
                default:
                  return Center(child: Text("404 : Page Not Found"));
              }
            }
        );
      },

    );
  }
}


