import 'Artist.dart';

class Album{
  String name;
  Artist artist;
  String cover;

  Album(this.name, this.artist, this.cover);
}
