import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spotify_clone/entities/Album.dart';
import 'package:spotify_clone/entities/Artist.dart';
import 'package:spotify_clone/pages/search.page.dart';

class HomeWidgetPage extends StatefulWidget {
  HomeWidgetPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _HomeWidgetPageState createState() => _HomeWidgetPageState();
}

class _HomeWidgetPageState extends State<HomeWidgetPage> {
  int _currentIndex = 0;
  final List<Album> albums = [
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
    Album("Nevermind", Artist("Nirvana"),
        'https://images-na.ssl-images-amazon.com/images/I/71DQrKpImPL._SL1400_.jpg'),
  ];

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
    });
  }

  Widget buildMoreFrom(Artist artist, List<Album> albums) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 8),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Image.network(
                    "https://cdns-images.dzcdn.net/images/artist/ca074e2c087515c221752184b1da25d0/264x264.jpg",
                    height: 75,
                    width: 75,
                  ),
                ),
                Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Plus du genre",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 15,
                            color: Colors.grey[500]
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        "Bratisla Boys",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.white
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              children: albums.map((album) => buildAlbumTile(album)).toList(),
            ),
          )
        ],
      ),
    );
  }

  Widget buildRecentlyListenedList() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 20),
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.all(8),
          child: Text(
            "Ecoutés Récemment",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        Container(
          height: 150,
          child: ListView(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            children: albums.map((album) => buildAlbumTile(album)).toList(),
          ),
        )
      ],
    );
  }

  Widget buildAlbumTile(Album album) {
    return Container(
      height: 150,
      width: 110.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Image.network(
            album.cover,
            fit: BoxFit.cover,
            width: 100,
            height: 100,
          )),
          Container(
            padding: const EdgeInsets.all(8),
            alignment: Alignment.centerLeft,
            child: Text(
              album.name,
              style: TextStyle(fontWeight: FontWeight.normal),
            ),
          )
        ],
      ),
    );
  }

  Widget buildTilesGrid() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(8),
          alignment: Alignment.centerLeft,
          child: Text(
            "Bon Après-midi",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
                child: Column(
              children: [
                buildAlbumSuggestion(albums[0].name, albums[0].cover),
                buildAlbumSuggestion(albums[1].name, albums[1].cover),
                buildAlbumSuggestion(albums[2].name, albums[2].cover),
              ],
            )),
            Expanded(
                child: Column(
              children: [
                buildAlbumSuggestion(albums[0].name, albums[0].cover),
                buildAlbumSuggestion(albums[1].name, albums[1].cover),
                buildAlbumSuggestion(albums[2].name, albums[2].cover),
              ],
            )),
          ],
        )
      ],
    );
  }

  Widget buildAlbumSuggestion(String name, String thumbnail) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: Colors.grey[800],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: ClipRRect(
                borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(10.0),
                ),
                child: Image.network(
                  thumbnail,
                  width: 60,
                  height: 60,
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  name,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              buildTilesGrid(),
              buildRecentlyListenedList(),
              buildMoreFrom(Artist("Bratisla Boys"), [
                Album("Stach Stach", Artist("Bratisla Boys"), "https://images-na.ssl-images-amazon.com/images/I/41B8ZK3YVSL.jpg"),
                Album("Stach Stach", Artist("Bratisla Boys"), "https://images-na.ssl-images-amazon.com/images/I/41B8ZK3YVSL.jpg"),
                Album("Stach Stach", Artist("Bratisla Boys"), "https://images-na.ssl-images-amazon.com/images/I/41B8ZK3YVSL.jpg"),
                Album("Stach Stach", Artist("Bratisla Boys"), "https://images-na.ssl-images-amazon.com/images/I/41B8ZK3YVSL.jpg"),])
            ],
          ),
        )
      ),
    );
  }
}
